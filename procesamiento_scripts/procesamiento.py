# -*- coding: utf-8 -*-

'''
Script que se encarga de unificar las rutinas de procesamiento de los archivos rinex. 

                           Servicio Geológico Colombiano 
                                      OVSM 
                               Área de deformación
          Este programa es software libre, para redistribuir y modificar.
   
                                 Requerimientos
 Verificar que se tenga instaladas las librerías: toolbet y argpase. En caso de no 
     tenerse intaladas se pueden instalar ejecutando los siguientes comandos: 
        
                          pip install requests-toolbelt
                              pip install argparse
                             
        Verificar que el script csrs_ppp_auto.py, carpeta_correspondiente.py y 
                  carpeta_unica.py se encuentren en la misma carpeta
                                que este script.
                                
                Para más información consultar el manual de usuario.
                
                        Autor: Alejandro Salazar González.
                        Autor código original: Joseph B.
      
'''
import os
from time import sleep 

os.system ("cls") 

print("    ")
print("                                       Servicio Geológico Colombiano")
print("                                                   OVSM")
print("    ")
print("                                            Área de deformación ")
print("                                      Procesamiento de archivos Rinex")
print("    ")
print("                    A continuación podrá ver las opciones disponibles para el procesamiento")
sleep(2)


print("       ")
print('Para procesar los archivos que se encuentran en una sola carpeta -----------------------------> 0')
print('Para procesar los archivos que se encuentran distribuidos en sus carpetas correspondientes ---> 1')

print("       ")

print("Digite su respuesta ")
i=input()  #Valor digitado por el usuario.
i=int(i)

if i==0:
    os.system("python procesamiento_scripts/carpeta_unica.py") #Si la respuesta es 0 ejecuta el código de carpeta única
elif i==1:
    os.system("python procesamiento_scripts/carpeta_correspondiente.py") #Si la respuesta es 1 ejecuta el código de carpeta correspondiente
else: 
    print("No se reconoció la respuesta, por favor intente nuevamente.")   
    exit()
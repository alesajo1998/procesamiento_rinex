# -*- coding: utf-8 -*-
'''
         Script para procesar una o varias estaciones en su carpeta correspondiente. 

                           Servicio Geológico Colombiano 
                                      OVSM 
                               Área de deformación
          Este programa es software libre, para redistribuir y modificar.
   
                                 Requerimientos
 Verificar que se tenga instaladas las librerías: toolbet y argpase. En caso de no 
     tenerse instaladas se pueden instalar ejecutando los siguientes comandos: 
        
                          pip install requests-toolbelt
                              pip install argparse
                             
       Verificar que el script csrs_ppp_auto.py se encuentre en la misma carpeta
                                que este script.
                                
                Para más información consultar el manual de usuario.
                
                        Autor: Alejandro Salazar González.
                        Autor código original: Joseph B.
      
'''
#Librerías. 
import os
from datetime import datetime
from time import time
from time import sleep 
import threading

#Aquí se encuentra la lista de estaciones que se encuentran disponibles. 
#En caso de tener otra estación disponible simplemente se agrega siguiendo la misma estructura.
lista_estaciones=('afri','agui','bis0','bllr','esme','gual','inde','lagu','lver','mral',
                  'mzal','nere','olle','otun','parm','pira','quim','recm','rode','rubi',
                  'sana','secr','sinn')

#Rutas. 
f = open ('procesamiento_scripts/configuracion_cc.txt','r')
rut = f.readlines()
f.close()

r =rut[0]
ruta=r.rstrip()

rl = rut[1]
ruta_log=rl.rstrip()

a=rut[2]
year= a.rstrip('\n')
year_end=year[-2:]   #Ultimos dos números del año

numbers=[]  #Vector que contiene los números de cada estación
checked_numbers=[] #Vector que contiene los números rectificados
estaciones=[] #Vector que contiene las estaciones seleccionadas.

lenght=len(lista_estaciones) #Total de las estaciones disponibles
print("                       Procesamiento de archivos Rinex en sus carpetas correspondientes")
print("    ")
print("                  A continuación podrá ver las estaciones disponibles para el procesamiento")

sleep(2)
print("       ")
print('Afri ----> 0')
print('Agui ----> 1')
print('Bis0 ----> 2')
print('Bllr ----> 3')
print('Esme ----> 4')
print('Gual ----> 5')
print('Inde ----> 6')
print('Lagu ----> 7')
print('Lver ----> 8')
print('Mral ----> 9')
print('Mzal ---> 10')
print('Nere ---> 11')
print('Olle ---> 12')
print('Otun ---> 13')
print('Parm ---> 14')
print('Pira ---> 15')
print('Quim ---> 16')
print('Recm ---> 17')
print('Rode ---> 18')
print('Rubi ---> 19')
print('Sana ---> 20')
print('Secr ---> 21')
print('Sinn ---> 22')
print("       ")
print("Para todas las estaciones ---> t")
print("       ")
print("Digite el número de la estación o de las estaciones que desee procesar separados por una coma.(Por ejemplo 1,2,3) ")
i=input()
longitud=len(i) #Longitud de la lista ingresada por el usuario.

e=0 #Contador
while e < longitud:  
    if i[e] != "t":          #Ciclo que empieza desde 0 hasta la logitud total de la lista.
         if i[e] != ",":               #Si el caracter es diferente a una coma 
             x=e+1                     #Analiza el caracter siguiente
             if x<longitud:            #Si el caracter siguiente existe
                 if i[x] != ",":       #Si el caracter siguiente es diferente a una coma significa que hay un número
                     n=i[e]+i[x]       #Si hay otro número significa que contiene un número de 2 cifras
                     n=int(n)          #Une los dos números y los convierte a enteros.
                     numbers.append(n) #Los agrega al vector de los números
                     e=e+2             #Y suma dos al contador
                 else:
                     n=i[e]            #Sino significa que solo hay un número
                     n=int(n)          #Lo convierte a entero y lo agrega al vector de los números
                     numbers.append(n)
                     e=e+1             #Suma 1 al contador
             else:
                 n=i[e]                #Sino significa que es el último número
                 n=int(n)              #Convierte el valor a entero 
                 numbers.append(n)     #Lo agrega al vector de los números
                 e=e+1                 #Suma 1 al contador
         else:  
             e=e+1 
    else:
        numbers.extend(range(0,lenght)) 
        e=longitud
      
            
for b in numbers:
    if b not in checked_numbers and b<lenght: #Pasamos los archivos del vector sin rectificar al rectificado.
        checked_numbers.append(b)             #Esto es por si hay un número repetido
    
for p in checked_numbers:                     #Pasamos todos los números a un nuevo vector con las estaciones correspondientes
    estaciones.append(lista_estaciones[p])

estaciones_print=str(estaciones)
#Fecha y hora tomada al momento de iniciar el código para el log.
fecha= datetime.now()
fecha=str(fecha)

#Cronómetro para saber la duración del programa.
TI=time()

global resumen
resumen='' #Resumen de los errores que presenta el programa. 

#Inicio del log.
log = open(ruta_log, "a") 
log.write("Log " + fecha + "\n")
log.close()

print ("Digite la fecha inicial (Calendario Juliano) ")
dia_inicio = int(input())

print ("Digite la fecha final (Calendario Juliano) ")
dia_fin = int(input())

os.system("cls") #Borra ventana de comandos.

#Copia en el log los días que serán procesados.
log = open(ruta_log, "a") 
log.write("Procesando desde el día "+ str(dia_inicio) + " hasta el día " + str(dia_fin) + " "+ "de "+ estaciones_print +" del año "+year +"\n")
log.close()

print("Procesando desde el día "+ str(dia_inicio) + " hasta el día " + str(dia_fin) + " "+ " de "+ estaciones_print+" del año "+year +"\n")

def proceso(dias):
    global resumen
    global ruta_inicial
    global ruta_final
    global carpeta_estacion    
    if estacion=="recm":
        carpeta_estacion="recio"
    elif estacion=="secr":
        carpeta_estacion="secre"
    else:
        carpeta_estacion=estacion

    if dias < 10:
        dia = "00" + str(dias)
    elif dias < 100:
        dia = "0" + str(dias)
    else:
        dia = str(dias)              
    
    ruta_inicial = ruta + carpeta_estacion + "/"+year+"/RINEX/" + estacion  + dia +  "0." +year_end +"o"
    ruta_final= ruta + carpeta_estacion +"/PDF_"+year+"/"      
   
    if os.path.isfile(ruta_final+estacion+dia+"0.pdf"): #Si se encuentran ya los PDF los elimina para procesar los nuevos
        os.remove(ruta_final+estacion+dia+"0.pdf")
            
               
    if os.path.isfile(ruta_inicial): #Si el rinex están en la ruta especificada, continúa.
        terminado = True
    else: # Si no se encuentra el archivo lo copia en el log y sigue con el siguiente archivo
        print('No se encontró el archivo ' + estacion + dia +  "0." +year_end +"o")
        log = open(ruta_log, "a") 
        log.write('No se encontró el archivo ' + estacion + dia +  "0." +year_end +"o"+ "\n")
        log.close()
        resumen=resumen+ estacion + dia +  "0." +year_end +"o"+' '
        terminado = False
                
    inicio=time()  #Cronómetro para procesamiento individual del archivo.   
    intento=0
    while(terminado and intento < 2): #Ciclo para ejecutar el programa de Canadá y tiene un límite de dos intentos por archivo
        print(threading.currentThread().getName()," > Ejecutando csrs_ppp_autp.py ...")
        print("---------------------------------------------------------------------------")
               
        # Ejecucion del comando
        intento = intento + 1
        os.system("python procesamiento_scripts/csrs_ppp_auto.py --user_name deformacionovsm@gmail.com --rnx " + ruta_inicial + " --results_dir " + ruta_final + " --mode Static --get_max 179")
               
        # Verificacion de exito en la descarga, elimina excedentes y copia en el log los tiempos.
        if os.path.isfile(ruta_final+estacion+dia+"0.pdf"):
            terminado = False
            print(threading.currentThread().getName()," > Eliminando excedentes ...")
            print(threading.currentThread().getName(),"---------------------------------------------------------------------------")
            os.remove(ruta_final + "\\" + estacion + dia + "0.sum")
            os.remove(ruta_final + "\\" + estacion + dia + "0_full_output.zip")
            final=time() #Finalización del cronómetro por archivo procesado.
            Parcialtime=(final-inicio)/60#Calcula el tiempo Parcial de dicho archivo.
            Parcialtime=round(Parcialtime,4) #Lo limita a 4 decimales.
            print(threading.currentThread().getName(),'Procesamiento exitoso de ' + estacion + dia +  "0." +year_end +"o"+ " Tiempo: " + str(Parcialtime) + ' min ' + "\n")
            log = open(ruta_log, "a")  #Copia el registro en el log.
            log.write('Procesamiento exitoso de ' + estacion + dia +  "0." +year_end +"o"+ " Tiempo: " + str(Parcialtime) + ' min ' + "\n")
            log.close() 
            if os.path.isfile(ruta + carpeta_estacion + "\\"+year+"\\RINEX\\Status.txt"): #Si se encuentra el archivo Status lo elimina. 
                os.remove(ruta + carpeta_estacion + "\\"+year+"\\RINEX\\Status.txt")                        
        else:
            print(threading.currentThread().getName(),"-ALERTA--ALERTA--ALERTA--ALERTA--ALERTA--ALERTA--ALERTA--ALERTA--ALERTA--ALERTA-")
            print(threading.currentThread().getName()," > Error en el procesamiento, reintentando ...")
            log = open(ruta_log, "a") #Si falla el procesamiento y el archivo existe lo copia en el log.
            log.write('Error en el procesamiento de ' + estacion + dia +  "0." +year_end +"o"+ " intento " + str(intento) + "\n")
            log.close()
            if intento==2:
                resumen=resumen+ estacion + dia +  "0." +year_end +"o"+' '
                terminado = True
    return resumen
if(dia_inicio>0 and dia_fin<366):
    for estacion in estaciones:
        for dia in range (dia_inicio, dia_fin+1,4):
            w = threading.Thread(target=proceso, name='Thread 1', args=[dia])
            w.start()
    
            if (dia+1)<= dia_fin:
                sleep(0.5)
                z = threading.Thread(target=proceso, name='Thread 2', args=[dia+1])
                z.start()
            
                if(dia+2)<=dia_fin:
                    sleep(0.5)
                    y= threading.Thread(target=proceso, name='Thread 3', args=[dia+2])
                    y.start()
                
                    if(dia+3)<=dia_fin:
                        sleep(0.5)
                        q=threading.Thread(target=proceso, name='Thread 4', args=[dia+3])
                        q.start()
                        q.join()
                    y.join()
                z.join()
            w.join()
                  
                 
    TF=time() #Finalización del cronómetro total del programa.
    Totaltime= (TF-TI)/60 #Calcula el tiempo total
    Totaltime=round(Totaltime,4) #Lo limita a 4 decimales.
    log = open(ruta_log, "a") #Copia el el log la finalización del programa.
    if resumen != '':    
        log.write('En resumen se encontraron problemas en: ' + resumen + "\n")
        print('En resumen se encontraron problemas en: ' + resumen) 
    else:
        log.write('En resumen no se encontraron problemas.' + "\n")
        print('En resumen no se encontraron problemas.')
    log.write('Procesamiento finalizado. Duración total: ' + str(Totaltime) + ' min ' +"\n"+ "\n")
    log.close()          
    print('Procesamiento finalizado. Duración total: ' + str(Totaltime) + ' min ')
                   
else:
    print("Rango de dí­as incorrecto.")
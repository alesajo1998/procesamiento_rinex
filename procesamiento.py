import tkinter as tk
from tkinter import ttk
from tkinter import font
from tkinter import filedialog
from PIL import ImageTk, Image
import webbrowser
import os


def center(toplevel):
    toplevel.update_idletasks()
    w = toplevel.winfo_screenwidth()
    h = toplevel.winfo_screenheight()
    size = tuple(int(_) for _ in toplevel.geometry().split('+')[0].split('x'))
    x = w/2 - size[0]/2
    y = h/2 - size[1]/2
    toplevel.geometry("%dx%d+%d+%d" % (size + (x, y)))
    
def repositorio():
    webbrowser.open("https://gitlab.com/alesajo1998/procesamiento_rinex/-/tree/master", new=2, autoraise=True)      

def ejecutar():
    main_window.iconify()
    os.system("python procesamiento_scripts/procesamiento.py") 
    

def read_cu():
    guardar_label_cu.grid_forget()
    cancel_label_cu.grid_forget() 
    f_cu = open ('procesamiento_scripts/configuracion_cu.txt','r')
    rut = f_cu.readlines()
    f_cu.close()
    rr_cu =rut[0]
    ruta_rinex_cus=rr_cu.rstrip()
    rf_cu = rut[1]
    ruta_final_cus=rf_cu.rstrip()
    rl_cu=rut[2]
    ruta_log_cus=rl_cu.rstrip()
    y_cu=rut[3]
    year_cus=y_cu.rstrip()
    
    ruta_rinex_cu.delete(0, 'end')
    ruta_final_cu.delete(0, 'end')
    ruta_log_cu.delete(0, 'end')
    year_cu.delete(0, 'end')
    
    ruta_rinex_cu.insert(0, ruta_rinex_cus)
    ruta_final_cu.insert(0, ruta_final_cus)
    ruta_log_cu.insert(0, ruta_log_cus)
    year_cu.insert(0, year_cus) 
    
    
def read_cc():
    guardar_label_cc.grid_forget()
    cancel_label_cc.grid_forget() 
    f_cc = open ('procesamiento_scripts/configuracion_cc.txt','r')
    rut = f_cc.readlines()
    f_cc.close()
    rp_cc =rut[0]
    ruta_procesamiento_ccs=rp_cc.rstrip()
    rl_cc=rut[1]
    ruta_log_ccs=rl_cc.rstrip()
    y_cc=rut[2]
    year_ccs=y_cc.rstrip()
    
    ruta_procesamiento_cc.delete(0, 'end')
    ruta_log_cc.delete(0, 'end')
    year_cc.delete(0, 'end')
    
    ruta_procesamiento_cc.insert(0, ruta_procesamiento_ccs)
    ruta_log_cc.insert(0, ruta_log_ccs)
    year_cc.insert(0, year_ccs) 
    
    
def ruta1_cu():
    guardar_label_cu.grid_forget()
    cancel_label_cu.grid_forget() 
    folder_selected = filedialog.askdirectory()
    if folder_selected == "":
        folder_selected=ruta_rinex_cu
    else: 
        ruta_rinex_cu.delete(0, 'end')
        folder_selected=folder_selected+"/"
        ruta_rinex_cu.insert(0, folder_selected)
    
def ruta1_cc():
    guardar_label_cc.grid_forget()
    cancel_label_cc.grid_forget() 
    folder_selected = filedialog.askdirectory()
    if folder_selected == "":
        folder_selected=ruta_procesamiento_cc
    else: 
        ruta_procesamiento_cc.delete(0, 'end')
        folder_selected=folder_selected+"/" 
        ruta_procesamiento_cc.insert(0, folder_selected)    
      
    
def ruta2_cu():
    guardar_label_cu.grid_forget()
    cancel_label_cu.grid_forget() 
    folder_selected = filedialog.askdirectory()
    if folder_selected == "":
        folder_selected=ruta_final_cu
    else:     
        ruta_final_cu.delete(0, 'end')
        folder_selected=folder_selected+"/" 
        ruta_final_cu.insert(0, folder_selected)   
    
def ruta2_cc():
    guardar_label_cc.grid_forget()
    cancel_label_cc.grid_forget()    
    folder_selected = filedialog.askdirectory()
    if folder_selected == "":
        folder_selected=ruta_log_cc
    else:     
        ruta_log_cc.delete(0, 'end') 
        folder_selected=folder_selected+"/" +"log.txt"   
        ruta_log_cc.insert(0, folder_selected)     
    
def ruta3_cu():
    guardar_label_cu.grid_forget()
    cancel_label_cu.grid_forget()  
    folder_selected = filedialog.askdirectory()
    if folder_selected == "":
        folder_selected=ruta_log_cu
    else:     
        ruta_log_cu.delete(0, 'end') 
        folder_selected=folder_selected+"/"+"log.txt"  
        ruta_log_cu.insert(0, folder_selected)   
    
def guardar_cu():
    rinex = ruta_rinex_cu.get()
    final = ruta_final_cu.get()
    log = ruta_log_cu.get()
    year = year_cu.get()
    f_cu = open ('procesamiento_scripts/configuracion_cu.txt','r+')
    f_cu.truncate(0)
    f_cu.close()
    f_cu = open ('procesamiento_scripts/configuracion_cu.txt','w')
    f_cu.write(rinex+"\n")
    f_cu.write(final+"\n")
    f_cu.write(log+"\n")
    f_cu.write(year+"\n")
    f_cu.close()    
    
    read_cu()
    cancel_label_cu.grid_forget() 
    guardar_label_cu.grid(row=20, column=0, sticky="w")
    
def cancelar_cu():
    read_cu()
    cancel_label_cu.grid(row=20, column=0, sticky="w")
    
    
def guardar_cc():
    procesamiento = ruta_procesamiento_cc.get()
    log = ruta_log_cc.get()
    year = year_cc.get()
    f_cc = open ('procesamiento_scripts/configuracion_cc.txt','r+')
    f_cc.truncate(0)
    f_cc.close()
    f_cc = open ('procesamiento_scripts/configuracion_cc.txt','w')
    f_cc.write(procesamiento+"\n")
    f_cc.write(log+"\n")
    f_cc.write(year+"\n")
    f_cc.close()    
    
    read_cc()
    cancel_label_cc.grid_forget() 
    guardar_label_cc.grid(row=20, column=0, sticky="w")
    
def cancelar_cc():
    read_cc()
    cancel_label_cc.grid(row=20, column=0, sticky="w")   
    
    

main_window = tk.Tk()
main_window.geometry("600x400")
main_window.title("Deformación")
main_window.iconphoto(False, tk.PhotoImage(file='icons/volcano2.png'))
center(main_window)

#Create notebook
notebook = ttk.Notebook(main_window)
notebook.pack(fill='both', expand='yes')

#Create tabs
tab1= ttk.Frame(notebook)
tab2= ttk.Frame(notebook)
tab3= ttk.Frame(notebook)

#Add tabs 
notebook.add(tab1 , text = 'Ejecutar')
notebook.add(tab2 , text = 'Configuración')
notebook.add(tab3 , text = 'Ayuda')
main_frame=tk.LabelFrame(tab2,borderwidth = 0, highlightthickness = 0)
main_frame.grid(row=0, column=0, sticky="n")
nb= ttk.Notebook(main_frame)
nb.pack()
tab4= ttk.Frame(nb)
tab5= ttk.Frame(nb)
nb.add(tab4 , text = 'Carpeta única')
nb.add(tab5 , text = 'Carpeta correspondiente')

#Ejecutar tab
ejecutar_frame=tk.LabelFrame(tab1,borderwidth = 0, highlightthickness = 0)
ejecutar_frame.grid(row=0, column=0, sticky="n")
img = Image.open("Icons/servicio.png")
img = img.resize((600, 300), Image.ANTIALIAS)
img = ImageTk.PhotoImage(img)
background = ttk.Label(ejecutar_frame, image = img)
background.grid(row=1, column=0,sticky="n")

#Boton ejecutar
boton_ejecutar=tk.Button(ejecutar_frame, text="Ejecutar procesamiento.py", command= ejecutar,  cursor="hand2", overrelief="raised", borderwidth=5)
boton_ejecutar.grid(row=2, column=0,sticky="n")



#Carpeta única
carpeta_unica_frame=tk.LabelFrame(tab4,borderwidth = 0, highlightthickness = 0, relief="flat")
carpeta_unica_frame.pack(padx=130,pady=30)

#Texto
LabelsFont = font.Font(family='DejaVu Sans', size=10, weight='bold')
LabelsFont2 = font.Font(family='DejaVu Sans', size=8)
rrcu= tk.Label(carpeta_unica_frame,font=LabelsFont,text="Ruta Rinex(Donde están los archivos rinex):",borderwidth = 0, highlightthickness = 0)
rrcu.grid(row=1, column=0, sticky="w")

#Recuadro
ruta_rinex_cu= tk.Entry(carpeta_unica_frame, width=52)
ruta_rinex_cu.grid(pady=4, row=2, column=0,sticky="w")
#ruta_rinex_cu.pack()

#Icono
fol = Image.open('icons/folder.png')
fol = fol.resize((20, 20), Image.ANTIALIAS) # Redimension (Alto, Ancho)
fol = ImageTk.PhotoImage(fol)
boton_folder=tk.Button(carpeta_unica_frame,image=fol, text="Presiona", command= ruta1_cu,  cursor="hand2")
boton_folder.grid(row=2, column=2,sticky="w")

#Espacio vacío
frame_vacio=tk.LabelFrame(carpeta_unica_frame,borderwidth = 0, highlightthickness = 0, relief="flat")
frame_vacio.grid(padx=0, pady= 15,row=3, column=0,sticky="n")

#Texto 
rfcu= tk.Label(carpeta_unica_frame,font=LabelsFont,text="Ruta final(Donde quedará el PDF):",borderwidth = 0, highlightthickness = 0)
rfcu.grid(pady=0, row=4, column=0, sticky="w")

#Recuadro
ruta_final_cu= tk.Entry(carpeta_unica_frame, width=52)
ruta_final_cu.grid(pady=4, row=5, column=0,sticky="w")

#Icono
boton_folder2=tk.Button(carpeta_unica_frame,image=fol, text="Presiona", command= ruta2_cu,  cursor="hand2")
boton_folder2.grid(row=5, column=2,sticky="w")

#Espacio vacío
frame_vacio2=tk.LabelFrame(carpeta_unica_frame,borderwidth = 0, highlightthickness = 0, relief="flat")
frame_vacio2.grid(padx=0, pady= 13,row=6, column=0,sticky="n")

#Texto 
rlcu= tk.Label(carpeta_unica_frame,font=LabelsFont,text="Ruta del log(Donde quedará el log):",borderwidth = 0, highlightthickness = 0)
rlcu.grid(row=7, column=0, sticky="w")

#Recuadro
ruta_log_cu= tk.Entry(carpeta_unica_frame, width=52)
ruta_log_cu.grid(pady=4, row=8, column=0,sticky="w")

#Icono
boton_folder3=tk.Button(carpeta_unica_frame,image=fol, command= ruta3_cu,  cursor="hand2")
boton_folder3.grid(row=8, column=2,sticky="w")

#Espacio vacío
frame_vacio3=tk.LabelFrame(carpeta_unica_frame,borderwidth = 0, highlightthickness = 0, relief="flat")
frame_vacio3.grid(padx=0, pady= 13,row=14, column=0,sticky="n")

#Texto 
ycu= tk.Label(carpeta_unica_frame,font=LabelsFont,text="Año de los archivos:",borderwidth = 0, highlightthickness = 0)
ycu.grid(row=16, column=0, sticky="n")

#Recuadro
year_cu= tk.Entry(carpeta_unica_frame, width=12, justify=tk.CENTER)
year_cu.grid(row=17, column=0,sticky="n")

#Espacio vacío
frame_vacio4=tk.LabelFrame(carpeta_unica_frame,borderwidth = 0, highlightthickness = 0, relief="flat")
frame_vacio4.grid(padx=0, pady= 13,row=18, column=0,sticky="n")

#Guardar 
boton_save_cu=tk.Button(carpeta_unica_frame,text="Guardar", command= guardar_cu,  cursor="hand2")
boton_save_cu.grid(padx=5, row=19, column=0,sticky="e")

#Cancelar
boton_cancelar_cu=tk.Button(carpeta_unica_frame,text="Cancelar", command= cancelar_cu, cursor="hand2")
boton_cancelar_cu.grid(row=19, column=2,sticky="e")

#label guardar 
guardar_label_cu=tk.Label(carpeta_unica_frame,font=LabelsFont2,text="Guardado con éxito",borderwidth = 0, highlightthickness = 0)

#label cancelar 
cancel_label_cu=tk.Label(carpeta_unica_frame,font=LabelsFont2,text="Cancelado con éxito",borderwidth = 0, highlightthickness = 0)

read_cu()




#Carpeta correspondiente
#Espacio vacío
carpeta_correspondiente_frame=tk.LabelFrame(tab5, borderwidth = 0, highlightthickness = 0, relief="flat")
carpeta_correspondiente_frame.grid(padx=130, pady= 30,row=0, column=0, sticky="w")

#Texto
#rrcc= tk.Label(carpeta_correspondiente_frame,font=LabelsFont,text="Ruta Rinex(Donde están los archivos rinex):",borderwidth = 0, highlightthickness = 0)
#rrcc.grid(row=0, column=0, sticky="w")

rrcc= tk.Label(carpeta_correspondiente_frame,font=LabelsFont,text="Ruta de la carpeta procesamiento:             ",borderwidth = 0, highlightthickness = 0)
rrcc.grid(row=1, column=0, sticky="w")

#Recuadro
ruta_procesamiento_cc= tk.Entry(carpeta_correspondiente_frame, width=52)
ruta_procesamiento_cc.grid(padx=0, pady=4, row=2, column=0,sticky="w")

#Icono
boton_folder4=tk.Button(carpeta_correspondiente_frame,image=fol, command= ruta1_cc,  cursor="hand2")
boton_folder4.grid(padx=1,row=2, column=2,sticky="w")

#Espacio vacío
frame_vacio6=tk.LabelFrame(carpeta_correspondiente_frame,borderwidth = 0, highlightthickness = 0, relief="flat")
frame_vacio6.grid(padx=0, pady= 15,row=3, column=0,sticky="n")

#Texto 
rlcc= tk.Label(carpeta_correspondiente_frame,font=LabelsFont,text="Ruta del log(Donde quedará el log):",borderwidth = 0, highlightthickness = 0)
rlcc.grid(row=4, column=0, sticky="w")

#Recuadro

ruta_log_cc= tk.Entry(carpeta_correspondiente_frame, width=52)
ruta_log_cc.grid(pady=4, row=5, column=0,sticky="w")

#Icono
boton_folder4=tk.Button(carpeta_correspondiente_frame,image=fol, command= ruta2_cc,  cursor="hand2")
boton_folder4.grid(padx=1,row=5, column=2,sticky="w")

#Espacio vacío
frame_vacio7=tk.LabelFrame(carpeta_correspondiente_frame,borderwidth = 0, highlightthickness = 0, relief="flat")
frame_vacio7.grid(padx=0, pady= 13,row=6, column=0,sticky="n")

#Texto 
ycc= tk.Label(carpeta_correspondiente_frame,font=LabelsFont,text="Año de los archivos:",borderwidth = 0, highlightthickness = 0)
ycc.grid(row=7, column=0, sticky="n")

#Recuadro

year_cc= tk.Entry(carpeta_correspondiente_frame, width=12, justify=tk.CENTER)
year_cc.grid(row=8, column=0,sticky="n")

#Espacio vacío
frame_vacio8=tk.LabelFrame(carpeta_correspondiente_frame,borderwidth = 0, highlightthickness = 0, relief="flat")
frame_vacio8.grid(padx=0, pady= 48,row=9, column=0,sticky="n")


#Guardar 
boton_save_cc=tk.Button(carpeta_correspondiente_frame,text="Guardar", command= guardar_cc,  cursor="hand2")
boton_save_cc.grid(padx=4,pady=1,  row=10, column=0,sticky="e")

#Cancelar
boton_cancelar_cc=tk.Button(carpeta_correspondiente_frame,text="Cancelar", command= cancelar_cc,  cursor="hand2")
boton_cancelar_cc.grid(padx=1,row=10, column=2,sticky="w")

#label guardar 
guardar_label_cc=tk.Label(carpeta_correspondiente_frame,font=LabelsFont2,text="Guardado con éxito",borderwidth = 0, highlightthickness = 0)

#label cancelar 
cancel_label_cc=tk.Label(carpeta_correspondiente_frame,font=LabelsFont2,text="Cancelado con éxito",borderwidth = 0, highlightthickness = 0)

read_cc()


#Ayuda tab

ayuda_frame=tk.LabelFrame(tab3,borderwidth = 0, highlightthickness = 0)
ayuda_frame.grid(row=0, column=0, sticky="n")

img3 = Image.open("Icons/help.png")
img3 = img3.resize((227, 300), Image.ANTIALIAS)
img3 = ImageTk.PhotoImage(img3)
img4 = Image.open("Icons/rep.png")
img4 = img4.resize((20, 20), Image.ANTIALIAS)
img4 = ImageTk.PhotoImage(img4)
background2 = ttk.Label(ayuda_frame, image = img3)
background2.grid(padx=180,row=0, column=0,sticky="n")

#Boton ayuda
boton_ayuda=tk.Button(ayuda_frame, text="Abrir repositorio", command= repositorio,  cursor="hand2")
boton_ayuda.grid(pady=20,row=2, column=0,sticky="n")

background3 = ttk.Label(ayuda_frame, image = img4)
background3.grid(padx=210,pady=20,row=2, column=0,sticky="e")

main_window.mainloop()